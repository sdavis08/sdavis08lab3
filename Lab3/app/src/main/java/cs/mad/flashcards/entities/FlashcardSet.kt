package cs.mad.flashcards.entities

data class FlashcardSet(val title: String) {
    companion object {
        fun getHardcodedFlashcardSets(): ArrayList<FlashcardSet> {
            val hardcoded = ArrayList<FlashcardSet>()
            for (i in 1..10) {
                hardcoded.add(FlashcardSet("Set $i"))
            }
            return hardcoded
        }
    }
}