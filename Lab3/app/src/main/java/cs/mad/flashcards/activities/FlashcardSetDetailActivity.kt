package cs.mad.flashcards.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.adapters.FlashcardAdapter
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardSet
import kotlin.random.Random

class FlashcardSetDetailActivity : AppCompatActivity() {

    private val output = Flashcard.getHardcodedFlashcards()
    private val caddapter = FlashcardAdapter(output)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_flashcard_set_detail)

        val crview = findViewById<RecyclerView>(R.id.Crview)

        crview.adapter = caddapter
        crview.layoutManager = LinearLayoutManager(this)
        crview.setHasFixedSize(true)
    }

    fun insertCard(view: android.view.View) {

        val index = Random.nextInt(9)

        val newItem = Flashcard("New Term", "New Definition")

        output.add(newItem)
        caddapter.notifyItemInserted(index)
    }
}