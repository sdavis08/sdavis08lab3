package cs.mad.flashcards.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.entities.FlashcardSet
import kotlin.random.Random
import android.content.Intent


/*
===================================================================================================================

     Reference documentation for recyclers: https://developer.android.com/guide/topics/ui/layout/recyclerview

===================================================================================================================
 */

class MainActivity : AppCompatActivity() {

    private val input = FlashcardSet.getHardcodedFlashcardSets()
    private val addapter = FlashcardSetAdapter(input)



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /*
            connect to views using findViewById
            setup views here - recyclerview, button
            don't forget to notify the adapter if the data set is changed
         */

        val rview = findViewById<RecyclerView>(R.id.Rview)


        rview.adapter = addapter
        rview.layoutManager = LinearLayoutManager(this)
        rview.setHasFixedSize(true)


    }

    fun insertItem(view: android.view.View) {

        val index = Random.nextInt(9)

        val newItem = FlashcardSet("New Set")

        input.add(newItem)
        addapter.notifyItemInserted(index)
    }

    fun viewSet(view: android.view.View) {

        val intent = Intent(this, FlashcardSetDetailActivity::class.java)
        startActivity(intent)
        //setContentView(R.layout.activity_flashcard_set_detail)

    }
}