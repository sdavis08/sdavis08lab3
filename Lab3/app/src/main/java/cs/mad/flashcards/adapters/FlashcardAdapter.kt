package cs.mad.flashcards.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.entities.Flashcard


class FlashcardAdapter(input: List<Flashcard>) : RecyclerView.Adapter<FlashcardAdapter.ViewHolder>() {

    private val myData = input

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        // store view references as properties using findViewById on view
        var ctview: TextView = view.findViewById<TextView>(R.id.Ctview)
        var ctview2: TextView = view.findViewById<TextView>(R.id.Ctview2)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_flashcard, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val ct = myData[position]

        holder.ctview.text = ct.term
        holder.ctview2.text = ct.definition
    }

    override fun getItemCount(): Int {
        return myData.size
    }

}